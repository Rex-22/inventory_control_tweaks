package net.sssubtlety.inventory_control_tweaks.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksUtil;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.getCONFIG;

@Environment(EnvType.CLIENT)
@Mixin(ClientPlayerInteractionManager.class)
public abstract class ClientPlayerInteractionManagerMixin {

    @Inject(method = "interactItem", cancellable = true,
            at = @At(value = "INVOKE", target = "Lnet/minecraft/util/TypedActionResult;getValue()Ljava/lang/Object;"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
    void swapIfArmorNotEmpty(PlayerEntity player, World world, Hand hand, CallbackInfoReturnable<ActionResult> cir, ItemStack usedStack, TypedActionResult<ItemStack> typedActionResult) {//ClientPlayNetworkHandler clientPlayNetworkHandler, Packet<?> packet, PlayerEntity player, World world, Hand hand) {
        final ActionResult result = typedActionResult.getResult();
        if (!getCONFIG().enableArmorSwap ||
                result == ActionResult.SUCCESS ||
                result == ActionResult.CONSUME)
            return;

        if (InventoryControlTweaksUtil.tryArmorSwap(player, usedStack, hand == Hand.MAIN_HAND ? player.getInventory().selectedSlot : PlayerInventory.OFF_HAND_SLOT))
            cir.setReturnValue(ActionResult.SUCCESS);
    }

}
