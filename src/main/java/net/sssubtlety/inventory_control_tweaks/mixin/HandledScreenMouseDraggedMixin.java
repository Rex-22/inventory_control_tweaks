package net.sssubtlety.inventory_control_tweaks.mixin;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.sssubtlety.inventory_control_tweaks.mixin_helpers.MouseDraggedMixinAccessor;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.*;
import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksUtil.*;

@Mixin(HandledScreen.class)
public abstract class HandledScreenMouseDraggedMixin<T extends ScreenHandler> extends Screen implements ScreenHandlerProvider<T>, MouseDraggedMixinAccessor {
    @Shadow protected boolean cursorDragging;

    @Shadow @Final protected T handler;
    @Shadow private boolean doubleClicking;
    private Inventory originInventory;
    private Inventory recentInventory;
    private boolean hasMovedStacks;

    protected HandledScreenMouseDraggedMixin(Text title) {
        super(title);
        throw new IllegalStateException("HandledScreenMouseDraggedMixin's dummy constructor called! ");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    void postConstruction(CallbackInfo info) {
        this.recentInventory = null;
        this.hasMovedStacks = false;
    }

    @Inject(method = "mouseDragged", locals = LocalCapture.CAPTURE_FAILHARD, at = @At(value = "INVOKE_ASSIGN",
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;getSlotAt(DD)Lnet/minecraft/screen/slot/Slot;"))
    void mouseDraggedTryMoveAll(double mouseX, double mouseY, int button, double deltaX, double deltaY, CallbackInfoReturnable<Boolean> cir, Slot hoveredSlot) {//, Slot hoveredSlot, ItemStack cursorStack) {
        if (this.client == null || this.doubleClicking || button != 0) return;

        boolean transferAll = isKeyPressed(dragAllStacksAcrossModifier, this.client);
        if (getCONFIG() != null &&
                (transferAll ||
                getCONFIG().dragMatchingStacksAcrossInventories)  &&
                (this.client.options != null) &&
                !this.client.options.touchscreen &&
                !this.cursorDragging &&
                hoveredSlot != null
        ) {
            ItemStack cursorStack = this.handler.getCursorStack();
            if (!cursorStack.isEmpty()) {
                if (recentInventory == null) {
                    originInventory = hoveredSlot.inventory;
                    recentInventory = originInventory;
                } else if (
                        !(hoveredSlot.inventory instanceof CraftingInventory && ((CraftingInventoryAccessor) hoveredSlot.inventory).getHandler() instanceof PlayerScreenHandler) &&
                                recentInventory != hoveredSlot.inventory) {
                    ClientPlayerEntity player = getClientPlayer();
                    if (player == null) return;

                    ClientPlayerInteractionManager interactionManager = getClientInteractionManager();
                    if (interactionManager == null) return;

                    if (transferAll) {
                        for (Slot handlerSlot : getCONFIG().chooseBottomRowStacksFirst ?
                                Lists.reverse(this.handler.slots) :
                                this.handler.slots) {
                            trySlotClick(player, recentInventory, interactionManager, button, SlotActionType.QUICK_MOVE, handler, handlerSlot,
                                    (stack) -> true, (slot_) -> handlerSlot.id);
                        }
                    } else {
                        clickEachMatchingSlot(
                                getCONFIG().chooseBottomRowStacksFirst ?
                                        Lists.reverse(this.handler.slots) :
                                        this.handler.slots,
                                player,
                                recentInventory,
                                cursorStack,
                                interactionManager,
                                button,
                                SlotActionType.QUICK_MOVE,
                                handler,
                                slot_ -> slot_.id);
                    }

                    recentInventory = hoveredSlot.inventory;
                    hasMovedStacks = true;
                }
            }
        }
    }

    @Override
    public Inventory getOriginInventory() {
        return originInventory;
    }

    @Override
    public Inventory getRecentInventory() {
        return recentInventory;
    }

    @Override
    public boolean hasMovedStacks() {
        return hasMovedStacks;
    }

    @Override
    public void endMove() {
        recentInventory = null;
        hasMovedStacks = false;
    }
}
