package net.sssubtlety.inventory_control_tweaks.mixin;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(CraftingInventory.class)
public interface CraftingInventoryAccessor {
//    @Accessor
//    DefaultedList<ItemStack> getStacks();

    @Accessor
    ScreenHandler getHandler();
}
