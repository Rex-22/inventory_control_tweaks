package net.sssubtlety.inventory_control_tweaks.mixin;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksConfig;
import net.sssubtlety.inventory_control_tweaks.mixin_helpers.MouseDraggedMixinAccessor;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.*;
import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksUtil.*;

@Mixin(HandledScreen.class)
public abstract class HandledScreenMouseReleasedMixin <T extends ScreenHandler> extends Screen implements ScreenHandlerProvider<T> {
    private static final int NO_SLOT_INDICATOR = -999;

    private MouseDraggedMixinAccessor draggedAccessor;

    @Shadow @Final protected T handler;
    @Shadow private boolean doubleClicking;

//    @Shadow @Final protected PlayerInventory playerInventory;

    protected HandledScreenMouseReleasedMixin(Text title) {
        super(title);
        throw new IllegalStateException("HandledScreenMouseReleasedMixin's dummy constructor called!");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    void postConstruction(CallbackInfo ci) {
        draggedAccessor = (MouseDraggedMixinAccessor) this;
    }

    @Inject(method = "mouseReleased", locals = LocalCapture.CAPTURE_FAILHARD, at = @At(value = "FIELD", ordinal = 2,
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;cancelNextRelease:Z"))
    void onMouseReleaseCancel(double mouseX, double mouseY, int button, CallbackInfoReturnable<Boolean> cir, Slot hoveredSlot, int i, int j, boolean bl, int slotIndicator) {
        if (this.client == null ||
                this.client.player == null ||
                button != 0 ||
                this.doubleClicking) return;

        final ItemStack cursorStack = this.client.player.currentScreenHandler.getCursorStack();

        if (slotIndicator == NO_SLOT_INDICATOR) {
            onReleaseOutsideBounds(cursorStack, getCONFIG());
        } else {
            if (hoveredSlot == null) return;
            onReleaseOverSlot(hoveredSlot, cursorStack, getCONFIG());
        }

        // reset unconditionally at end
        draggedAccessor.endMove();

    }

    private void onReleaseOutsideBounds(ItemStack cursorStack, InventoryControlTweaksConfig config) {
        if (cursorStack.isEmpty()) return;

        final boolean shouldThrowMatchingStacks = config.dragMatchingStacksOutOfInventory == InventoryControlTweaksConfig.Keyable.NO_KEY ||
                (config.dragMatchingStacksOutOfInventory == InventoryControlTweaksConfig.Keyable.WITH_KEY &&
                        isKeyPressed(dragOutOfInventoryModifier, this.client));

        if (!(shouldThrowMatchingStacks || config.dragSingleStackOutOfInventory)) return;

        final boolean isCreativeInv = this.client.player.currentScreenHandler instanceof CreativeInventoryScreen.CreativeScreenHandler;
        if (isCreativeInv) {
//            final ItemStack cursorStack1 = this.client.player.currentScreenHandler.getCursorStack();
            this.client.player.dropItem(cursorStack, true);
            this.client.interactionManager.dropCreativeStack(cursorStack);
            this.client.player.currentScreenHandler.setCursorStack(ItemStack.EMPTY);
        } else {
            clickSlotId(NO_SLOT_INDICATOR, SlotActionType.PICKUP);
        }

        if (shouldThrowMatchingStacks) {
            clickEachMatchingSlot(
                    this.handler.slots,
                    this.client.player,
                    draggedAccessor.getRecentInventory(),
                    cursorStack,
                    this.client.interactionManager,
                    1,
                    SlotActionType.THROW,
                    handler,
                    isCreativeInv ?
                        slot -> ((SlotAccessor)slot).getIndex() :
                        slot -> slot.id);
        }
    }

    private void onReleaseOverSlot(Slot hoveredSlot, ItemStack cursorStack, InventoryControlTweaksConfig config) {
        if (config.depositCursorStackOnRelease == InventoryControlTweaksConfig.DepositType.DISABLED ||
                !(config.dragMatchingStacksAcrossInventories &&
                draggedAccessor.hasMovedStacks()))
            return;

        Inventory recentInventory = draggedAccessor.getRecentInventory();
        if (recentInventory != hoveredSlot.inventory) return;
        if (config.depositCursorStackOnRelease == InventoryControlTweaksConfig.DepositType.AT_HOVERED_SLOT) {
            final ItemStack hoveredStack = hoveredSlot.getStack();
            if (hoveredStack.isEmpty() || stacksMatch(hoveredStack,cursorStack,false))
                clickSlotId(hoveredSlot.id, SlotActionType.PICKUP);
        }
        if (!cursorStack.isEmpty()) {
            // AT_EMPTY_SLOT or stack didn't fit AT_HOVERED_SLOT
            insertInEmptySlot(cursorStack, recentInventory);
        }

        if (!cursorStack.isEmpty()) {
            // cursor stack didn't fit
            final Inventory originInventory = draggedAccessor.getOriginInventory();
            if (recentInventory != originInventory)
                insertInEmptySlot(cursorStack, originInventory);

        }
    }

    private void insertInEmptySlot(ItemStack stack, Inventory destinationInventory) {
        for (Slot slot : destinationInventory == this.client.player.getInventory() ?
                Lists.reverse(this.handler.slots) : this.handler.slots) {
            ItemStack slotStack = slot.getStack();
            if (slot.inventory == destinationInventory &&
                    (slotStack.isEmpty() ||
                            (!stackIsFull(slotStack) && stacksMatch(slotStack, stack, false)))) {
                clickSlotId(slot.id, SlotActionType.PICKUP);
                if (stack.isEmpty()) break;
            }
        }
    }

    private void clickSlotId(int slotId, SlotActionType actionType) {
        this.client.interactionManager.clickSlot(this.handler.syncId, slotId, 0, actionType, this.client.player);
    }
}
