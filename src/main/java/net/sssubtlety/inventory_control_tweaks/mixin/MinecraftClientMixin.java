package net.sssubtlety.inventory_control_tweaks.mixin;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.WindowEventHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.thread.ReentrantThreadExecutor;
import net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksConfig;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static net.minecraft.entity.player.PlayerInventory.isValidHotbarIndex;
import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.getCONFIG;
import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksUtil.*;

@Mixin(MinecraftClient.class)
public abstract class MinecraftClientMixin extends ReentrantThreadExecutor<Runnable> implements WindowEventHandler {

    @Shadow @Nullable public ClientPlayerEntity player;
    @Shadow @Nullable public ClientPlayerInteractionManager interactionManager;

    public MinecraftClientMixin(String string) {
        super(string);
        throw new IllegalStateException("MinecraftClientMixin's dummy constructor called! ");
    }

    @Inject(method = "doItemPick", cancellable = true, locals = LocalCapture.CAPTURE_FAILEXCEPTION,
            at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/PlayerInventory;addPickBlock(Lnet/minecraft/item/ItemStack;)V"))
    void preAddPickBlock(CallbackInfo ci, boolean bl, BlockEntity be,
                         ItemStack pickedStack, HitResult.Type ht, PlayerInventory inventory, int matchInd) {
        // this method handles creative
        InventoryControlTweaksConfig config = getCONFIG();
        if (!config.enablePickFillsStack && !config.enablePickNeverChangesSlot) return;
        if (matchInd < 0) {
            // give 1
            ItemStack mainHandStack = player.getMainHandStack();
            if (mainHandStack.isEmpty())
                // vanilla works
                return;
            
            int emptyHotbarInd = getEmptyHotbarInd();
            if (emptyHotbarInd >= 0 &&
                // stack in hand, space in hotbar
                    config.enablePickNeverChangesSlot)
            {
                swapMainHandWithSlot(emptyHotbarInd);
                if (getEmptyHotbarInd() < player.getInventory().selectedSlot) {
                    // there's another empty slot before main hand,
                    //   vanilla won't work (will select different slot)

                    int originalSelectedSlot = inventory.selectedSlot;
                    // this simulates vanilla behavior
                    inventory.addPickBlock(pickedStack);
                    this.interactionManager.clickCreativeStack(this.player.getStackInHand(Hand.MAIN_HAND), getFirstHotbarInd() + inventory.selectedSlot);
                    // then swap new stack (in main hand) to original main hand slot
                    swapMainHandWithSlot(originalSelectedSlot);
                    // and scroll back to original main hand slot
                    scrollHotbarTo(originalSelectedSlot);

                    ci.cancel();
                }
            }
            // from here, vanilla works
            return;
        }

        if (pickBlockImpl(pickedStack, matchInd, config))
            fillMainHandCreative();

        ci.cancel();
    }

    @Inject(method = "doItemPick", cancellable = true, locals = LocalCapture.CAPTURE_FAILEXCEPTION,
            at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/PlayerInventory;isValidHotbarIndex(I)Z"))
    void preIsValidHotbarIndex(CallbackInfo ci, boolean bl, BlockEntity be,
            ItemStack pickedStack, HitResult.Type ht, PlayerInventory inventory, int matchInd) {
        // this method handles survival
        InventoryControlTweaksConfig config = getCONFIG();
        if (!config.enablePickNeverChangesSlot && !config.enablePickFillsStack) return;

        // All combinations with I, survival, do nothing regardless
        if (matchInd < 0) ci.cancel();
        pickBlockImpl(pickedStack, matchInd, config);
        ci.cancel();
    }

    private boolean pickBlockImpl(ItemStack pickedStack, int matchInd, InventoryControlTweaksConfig config) {
        // returns true if should try to fill stack in creative [a/b/c/d) 1/2)]
        ItemStack mainHandStack = player.getMainHandStack();
        if (matchInd == player.getInventory().selectedSlot || stacksMatch(mainHandStack, pickedStack, false)) {
            // a) or b): match in main hand
            //   a+b) don't care about I/II/III/IV
            if (config.enablePickFillsStack) {
                // 1/2), a/b): do nothing or fill
                tryFillMainHand(mainHandStack);
                return true;
            }
            // else config.enablePickNeverChangesSlot is guaranteed
            //   3), a/b): do nothing
            return false;
        } else if (mainHandStack.isEmpty()) {
            // d): holding nothing
            if (config.enablePickNeverChangesSlot || !isValidHotbarIndex(matchInd))
                // 1/3) d) II/III) OR 2) d) III): swap main hand and match
                swapMainHandWithSlot(matchInd);
            else
                // config.enablePickFillsStack is guaranteed
                // 2) d) II): scroll to match
                scrollHotbarTo(matchInd);
        } else {
            // c)
            if (isValidHotbarIndex(matchInd)) {
                // c) II)
                if (!config.enablePickNeverChangesSlot) {
                    // c) II) 2): Scroll to select match
                    scrollHotbarTo(matchInd);
                } else {
                    // c) II 1/3): // Swap match with main hand
                    swapMainHandWithSlot(matchInd);
                }
            } else {
                // c) III/IV)
                int emptyHotbarInd = getEmptyHotbarInd();
                if (emptyHotbarInd < 0) {
                    // c) IV) 1/2/3): Swap match with main hand
                    swapMainHandWithSlot(matchInd);
                } else {
                    // c) III)
                    if (!config.enablePickNeverChangesSlot) {
                        // c) III) 2): Move match to hotbar, scroll to select it
                        swapHotbarSlotWithSlot(emptyHotbarInd, matchInd);
                        scrollHotbarTo(emptyHotbarInd);
                    } else {
                        // c) III 1/3): // Move main-hand to other hotbar space, move match to main hand
                        swapMainHandWithSlot(emptyHotbarInd);
                        swapMainHandWithSlot(matchInd);
                    }
                }
            }
        }
        // both c and d get here
        return false;
    }

    private int getEmptyHotbarInd() {
        int emptyHotbarInd = -1;
        final int hotbarSize = PlayerInventory.getHotbarSize();
        for (int i = 0; i < hotbarSize; i++) {
            if (player.getInventory().getStack(i).isEmpty()) {
                emptyHotbarInd = i;
                break;
            }
        }
        return emptyHotbarInd;
    }

    private boolean tryAlternateBehavior(int matchIndex) {
        if (!isValidHotbarIndex(matchIndex) || player == null)
            return false;

        ItemStack mainHandStack = player.getMainHandStack();

        if (tryFillMainHand(mainHandStack))
            return true;
        else
            return tryPreventSlotChange(matchIndex, mainHandStack);
    }

    private boolean tryPreventSlotChange(int matchIndex, ItemStack mainHandStack) {
        if (!getCONFIG().enablePickNeverChangesSlot) return false;

        if (!pickedBlockIsInHand(matchIndex, mainHandStack)) {
            // swap current main-hand stack with matching stack instead of selecting the matching stack
            this.interactionManager.clickSlot(player.playerScreenHandler.syncId,
                    getClickedSlotId(matchIndex),
                    player.getInventory().selectedSlot,
                    SlotActionType.SWAP,
                    player);
        }
        return true;
    }

    private boolean tryFillMainHand(ItemStack mainHandStack) {
        if (!getCONFIG().enablePickFillsStack) return false;

        // combine each other matching stacks until main-hand stack is full
        final PlayerInventory inventory = player.getInventory();
        for (int i = 0; i < inventory.main.size() && !stackIsFull(mainHandStack); i++) {
            if (i == inventory.selectedSlot) continue;

            if (stacksMatch(inventory.main.get(i), mainHandStack, false))
                combineStacks(player, interactionManager, getClickedSlotId(i), getClickedSlotId(inventory.selectedSlot));
        }

        return true;
    }

    void fillMainHandCreative() {
        // fill stack by giving (cloning)
        if (!stackIsFull(player.getMainHandStack())) {
            final PlayerInventory inventory = player.getInventory();
            final int firstHotbarInd = getFirstHotbarInd();
            interactionManager.clickSlot(player.playerScreenHandler.syncId, firstHotbarInd + inventory.selectedSlot, 2, SlotActionType.CLONE, player);
            interactionManager.clickCreativeStack(ItemStack.EMPTY, firstHotbarInd + inventory.selectedSlot);
            interactionManager.clickSlot(player.playerScreenHandler.syncId, firstHotbarInd + inventory.selectedSlot, 0, SlotActionType.PICKUP, player);
        }
    }

    private void swapMainHandWithSlot(int otherInd) {
        swapHotbarSlotWithSlot(player.getInventory().selectedSlot, otherInd);
    }

    private void swapHotbarSlotWithSlot(int hotbarInd, int otherInd) {
        interactionManager.clickSlot(player.playerScreenHandler.syncId,
                getFirstHotbarInd() + hotbarInd, otherInd, SlotActionType.SWAP, player);
    }

    private void scrollHotbarTo(int slotInd) {
        scrollHotbarBy(player.getInventory().selectedSlot - slotInd);
    }

    private void scrollHotbarBy(int scrollAmount) {
        int scrollDirection = 1;
        if (scrollAmount < 0) {
            scrollDirection *= -1;
            scrollAmount *= -1;
        }

        for (; scrollAmount > 0; scrollAmount--)
            player.getInventory().scrollInHotbar(scrollDirection);
    }

    private boolean pickedBlockIsInHand(int matchIndex, ItemStack mainHandStack) {
        return matchIndex == player.getInventory().selectedSlot || stacksMatch(player.getInventory().main.get(matchIndex), mainHandStack, false);
    }
}
//    ALREADY_FULL,       // a)
//    HOLDING_PARTIAL,    // b)
//    HOLDING_OTHER,      // c)
//    HOLDING_NOTHING     // d)

//        0) inventory state (when match not in hand, applies to c) and d)
//           I)     No match
//           II)    Match on hotbar
//           III)   Match in inventory, hotbar has space
//           IV)    Match in inventory, no hotbar space

//           [creative completely fill stack]

//        1) both enabled:
//           a)     Do nothing
//          [b]     Fill from other stacks
//           cI)    Do nothing [move other to hotbar, inventory, or delete, give match to main hand]
//           cII)   Swap main hand with match
//           cIII)  Move main-hand to other hotbar space, move match to main hand
//           cIV)   Swap match with main hand
//           dI)    Do nothing [give match with 1]
//           dII)   Swap main hand with match
//           dIII)  Swap main hand with match
//           dIV)   impossible

//        2) just fill stack enabled
//           a)     Do nothing
//          [b]     Fill from other stacks
//           cI)   Do nothing [give match to hotbar if space, then scroll to select it;
//                    otherwise move other to inventory or delete, then give match to main hand]
//           cII)   Scroll to select match
//           cIII)  Move match to hotbar, scroll to select it
//           cIV)   Swap match with main hand
//           dI)    Do nothing [give match with 1]
//           dII)   Scroll to select match
//           dIII)  Swap match to main hand
//           dIV)   impossible

//        3) just prevent selection change
//           a)     Do nothing
//           b)     Do nothing
//           cI)    Do nothing [move other to hotbar, inventory, or delete]
//           cII)   Swap main hand with match
//           cIII)  Move main-hand to other hotbar space, move match to main hand
//           cIV)   Swap match with main hand
//           dI)    Do nothing [give match with 1]
//           dII)   Swap main hand with match
//           dIII)  Swap main hand with match
//           dIV)   impossible