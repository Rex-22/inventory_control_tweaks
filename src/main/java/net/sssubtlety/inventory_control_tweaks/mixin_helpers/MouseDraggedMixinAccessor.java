package net.sssubtlety.inventory_control_tweaks.mixin_helpers;

import net.minecraft.inventory.Inventory;

public interface MouseDraggedMixinAccessor {
    Inventory getOriginInventory();
    Inventory getRecentInventory();
    boolean hasMovedStacks();
    void endMove();
}
