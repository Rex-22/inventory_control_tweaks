package net.sssubtlety.inventory_control_tweaks;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.MOD_ID;

@Config(name = MOD_ID)
public class InventoryControlTweaksConfig implements ConfigData {
    @ConfigEntry.Gui.Excluded
    private static final Logger LOGGER = LogManager.getLogger();

    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean enableArmorSwap = true;
    @ConfigEntry.Gui.PrefixText

    @ConfigEntry.Gui.Tooltip(count = 3)
    private final List<String> armorSwapBlackList = new ArrayList<>();

    @ConfigEntry.Gui.Tooltip
    public boolean enableTranslationFetching = true;

    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean enablePickFillsStack = true;

    @ConfigEntry.Gui.Tooltip(count = 3)
    public boolean enablePickNeverChangesSlot = true;

    // drag_item_tweaks
    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 3)
    public boolean dragMatchingStacksAcrossInventories = true;
    @ConfigEntry.Gui.PrefixText

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean chooseBottomRowStacksFirst = true;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 2)
    public DepositType depositCursorStackOnRelease = DepositType.AT_EMPTY_SLOT;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean dragSingleStackOutOfInventory = true;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 4)
    public Keyable dragMatchingStacksOutOfInventory = Keyable.WITH_KEY;

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip()
    public boolean ignoreStackNbt = false;

    // offhand tweaks
    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 3)
    public boolean enableShiftClickToOffhandStack = true;

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean allFoodIsOffhandPreferred = true;

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean excludeFoodWithNegativeEffects = true;

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip(count = 3)
    private final List<String> offhandPreferredItems = Lists.newArrayList(
            Registry.ITEM.getId(Items.SHIELD).toString(),
            Registry.ITEM.getId(Items.FIREWORK_ROCKET).toString(),
            Registry.ITEM.getId(Items.TOTEM_OF_UNDYING).toString()
    );

    public void validatePostLoad() {
        InventoryControlTweaksClientInit.armorSwapBlackList = buildItemSetFromStrings(armorSwapBlackList);
        InventoryControlTweaksClientInit.offHandPreferredItemsSet = buildItemSetFromStrings(offhandPreferredItems);
    }

    private ImmutableSet<Item> buildItemSetFromStrings(List<String> itemStrings) {
        ImmutableSet.Builder<Item> itemSetBuilder = ImmutableSet.builder();
        for (String string : itemStrings) {
            Identifier id = new Identifier(string);

            Item item = Registry.ITEM.get(id);
            if (item == Items.AIR)
                LOGGER.warn("No item found for id " + id.toString());
            else
                itemSetBuilder.add(item);
        }

        return itemSetBuilder.build();
    }

    public enum DepositType {
        DISABLED,
        AT_HOVERED_SLOT,
        AT_EMPTY_SLOT
    }

    public enum Keyable {
        DISABLED,
        NO_KEY,
        WITH_KEY
    }
}
