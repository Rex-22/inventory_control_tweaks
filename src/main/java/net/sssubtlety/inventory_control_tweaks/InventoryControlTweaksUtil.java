package net.sssubtlety.inventory_control_tweaks;

import com.mojang.datafixers.util.Pair;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.world.GameMode;
import net.sssubtlety.inventory_control_tweaks.mixin.LivingEntityAccessor;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.armorSwapBlackList;
import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaksClientInit.getCONFIG;

public interface InventoryControlTweaksUtil {

    static ClientPlayerEntity getClientPlayer() {
        return MinecraftClient.getInstance().player;
    }

    static ClientPlayerInteractionManager getClientInteractionManager() {
        return MinecraftClient.getInstance().interactionManager;
    }

    static int getFirstHotbarInd() {
        return PlayerInventory.MAIN_SIZE;
    }
    
    static boolean stackIsFull(ItemStack stack) {
        return stack.getCount() == stack.getMaxCount();
    }

    static boolean stacksMatch(ItemStack stackA, ItemStack stackB, boolean ignoreNbt) {
        return ItemStack.areItemsEqualIgnoreDamage(stackA, stackB) &&
                (ignoreNbt || ItemStack.areNbtEqual(stackA, stackB));
    }

    static int getClickedSlotId(int slotInd) {
        return slotInd < PlayerInventory.getHotbarSize() ? slotInd + PlayerInventory.MAIN_SIZE : slotInd;
    }

    static int getClickedStackInd(int slotInd) {
        // index: 9-35, 0-8
        // clickedStackInd: 9-35, 36-44
        final int firstHotbarInd = getFirstHotbarInd();
        return slotInd >= firstHotbarInd ? slotInd - firstHotbarInd : slotInd;
    }

    static boolean isValidFood(Item item) {
        boolean isValid = item.isFood();
        if(isValid && getCONFIG().excludeFoodWithNegativeEffects && item != Items.SUSPICIOUS_STEW) {
            // item.isFood() does item.getFoodComponent() != null, so below is safe access
            List<Pair<StatusEffectInstance, Float>> statusEffects = item.getFoodComponent().getStatusEffects();
            if (statusEffects != null) {
                for (Pair<net.minecraft.entity.effect.StatusEffectInstance, Float> pair : statusEffects) {
                    if (!pair.getFirst().getEffectType().isBeneficial()) {
                        isValid = false;
                        break;
                    }
                }
            }
        }
        return isValid;
    }

    static void combineStacks(ClientPlayerEntity player, ClientPlayerInteractionManager interactionManager, int fromSlotId, int toIndex) {
        // should only be called of stacks are already known to match
        interactionManager.clickSlot(player.playerScreenHandler.syncId, fromSlotId, 0, SlotActionType.PICKUP, player);
        interactionManager.clickSlot(player.playerScreenHandler.syncId, toIndex, 0, SlotActionType.PICKUP, player);
        interactionManager.clickSlot(player.playerScreenHandler.syncId, fromSlotId, 0, SlotActionType.PICKUP, player);
    }

//    static void forEachSlot(List<Slot> slots, Predicate<Slot> slotCheck, BiPredicate<Slot, ItemStack> stackCheck, Consumer<Slot> slotAction, BooleanSupplier breakCondition) {
//        for (Slot slot : slots) {
//            if (slot != null && slotCheck.test(slot))
//            {
//                ItemStack curStack = slot.getStack();
//                if (stackCheck.test(slot, curStack))
//                    slotAction.accept(slot);
//
//            }
//        }
//    }

    static void clickEachMatchingSlot(List<Slot> slots, ClientPlayerEntity player, Inventory inventory, ItemStack referenceStack, ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler, Function<Slot, Integer> indexer) {
        for (Slot slot : slots) {
            trySlotClick(player, inventory, interactionManager, button, action, handler, slot,
                    (stack) -> stacksMatch(referenceStack, stack, getCONFIG().ignoreStackNbt), indexer);
        }
    }

    static void trySlotClick(ClientPlayerEntity player, Inventory inventory, ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler, Slot slot, Predicate<ItemStack> stackCheck, Function<Slot, Integer> indexer) {
        if (slot != null &&
                slot.canTakeItems(player) &&
                slot.hasStack() &&
                slot.inventory == inventory)
        {
            ItemStack curStack = slot.getStack();
            // stacksMatch is necessary in addition to canInsertItemIntoSlot
            if (stackCheck.test(curStack) &&
                    ScreenHandler.canInsertItemIntoSlot(slot, curStack, true))
            {
                interactionManager.clickSlot(handler.syncId, indexer.apply(slot), button, action, player);
            }
        }
    }

    static boolean tryArmorSwap(PlayerEntity player, ItemStack usedStack, int sourceSlotId) {
        if (armorSwapBlackList.contains(usedStack.getItem()))
            return false;

        EquipmentSlot equipmentSlot = MobEntity.getPreferredEquipmentSlot(usedStack);
        if (equipmentSlot == EquipmentSlot.MAINHAND)
            return false;

        int equipmentSlotId = equipmentSlot.getEntitySlotId();

        ClientPlayerInteractionManager interactionManager = MinecraftClient.getInstance().interactionManager;
        if (interactionManager == null)
            return false;

        interactionManager.clickSlot(player.playerScreenHandler.syncId, 8 - equipmentSlotId, sourceSlotId,
                SlotActionType.SWAP, player);
        ((LivingEntityAccessor) player).callOnEquipStack(usedStack);
        return true;
    }

    static boolean isKeyPressed(KeyBinding key, MinecraftClient client) {
        return InputUtil.isKeyPressed(client.getWindow().getHandle(),
                KeyBindingHelper.getBoundKeyOf(key).getCode());
    }

//    static GameMode getGameMode(PlayerEntity player) {
//        return MinecraftClient.getInstance().getNetworkHandler().getPlayerListEntry(player.getGameProfile().getId()).getGameMode();
//    }
}
