package net.sssubtlety.inventory_control_tweaks;

import com.google.common.collect.ImmutableSet;
import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.item.Item;
import org.lwjgl.glfw.GLFW;

public class InventoryControlTweaksClientInit implements ClientModInitializer {
	public static final String MOD_ID = "inventory_control_tweaks";

	private static InventoryControlTweaksConfig CONFIG;

	public static ImmutableSet<Item> armorSwapBlackList;

	public static ImmutableSet<Item> offHandPreferredItemsSet;

	public static InventoryControlTweaksConfig getCONFIG() {
		return CONFIG;
	}

	public static final KeyBinding dragOutOfInventoryModifier = KeyBindingHelper.registerKeyBinding(new KeyBinding(
			"key." + MOD_ID + ".drag_out_of_inventory_modifier",
			InputUtil.Type.KEYSYM,
			GLFW.GLFW_KEY_LEFT_CONTROL,
			"category." + MOD_ID
	));

	public static final KeyBinding clickArmorSwapModifier = KeyBindingHelper.registerKeyBinding(new KeyBinding(
			"key." + MOD_ID + ".click_armor_swap_modifier",
			InputUtil.Type.KEYSYM,
			GLFW.GLFW_KEY_RIGHT_SHIFT,
			"category." + MOD_ID
	));

	public static final KeyBinding dragAllStacksAcrossModifier = KeyBindingHelper.registerKeyBinding(new KeyBinding(
			"key." + MOD_ID + ".drag_all_stacks_across_modifier",
			InputUtil.Type.KEYSYM,
			GLFW.GLFW_KEY_LEFT_ALT,
			"category." + MOD_ID
	));

	@Override
	public void onInitializeClient() {
		AutoConfig.register(InventoryControlTweaksConfig.class, GsonConfigSerializer::new);
		CONFIG = AutoConfig.getConfigHolder(InventoryControlTweaksConfig.class).getConfig();

		if (CONFIG.enableTranslationFetching)
			CrowdinTranslate.downloadTranslations("inventory-control-tweaks", MOD_ID);
	}

}
