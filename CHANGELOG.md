- 1.3.13 (23 Mar. 2022): Marked as compatible with 1.18.2
- 1.3.12 (12 Dec. 2021): Marked as compatible with 1.18.1
- 1.3.11 (7 Dec. 2021): 
  
  Fixed compatibility with [Don't Drop It!](https://www.curseforge.com/minecraft/mc-mods/dont-drop-it) versions 2.3.6 and up!

- 1.3.10 (1 Dec. 2021): Updated for 1.18!
- 1.3.9 (7 Nov. 2021): 
  - the default keybinding for "Click to swap armor modifier" is now right-shift (was left-shift, which prevented snaking without the help of other mods)
  - [amecs](https://www.curseforge.com/minecraft/mc-mods/amecs) is now recommended. 
  [amecs](https://www.curseforge.com/minecraft/mc-mods/amecs) allows for multiple bindings to the same key, so it'll allow you to sneak even if you bind "Click to swap armor modifier" to left-shift.
- updated dependencies

- 1.3.8 (23 Jul. 2021): 
  No longer requires autoconfig, as it's built into cloth config now. 
  Thanks to [J. Fronny](https://gitlab.com/JFronny) for this update! 
- 1.3.7 (9 Jul. 2021): Marked as compatible with 1.17.1.
- 1.3.6 (2 Jul. 2021): Updated for 1.17!
- 1.3.5 (6 Jun. 2021): Fixed some issues when shift clicking things in a creative tab
- 1.3.4 (16 Apr. 2021): Two fixes:
  - Fixed strange behavior when dragging items across the player's 2x2 inventory crafting grid
  - Fixed dragging stacks out of inventory in creative with no external inventory open
- 1.3.3 (26 Mar. 2021): Fixed a strange crash that occurred when many other specific mods were present. 
- 1.3.2 (26 Mar. 2021): Fixed shift-clicking to offhand. 
  Another conflict has been reported with [Architectury](https://www.curseforge.com/minecraft/mc-mods/architectury-fabric), but I can't reproduce it. 
  If you have more information please post it in [this issue](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues/6). 
- 1.3.1 (25 Mar. 2021): Hotfix for crash when using this mod with REI (possibly other mods as well).
- 1.3 (23 Mar. 2021): 

  New features:
  - Hold the 'Click to swap armor modifier' key and click armor to swap it while your inventory's open (thanks to [Xpopy](https://www.curseforge.com/members/xpopy/projects) for the idea!)
  - Hold the 'Drag all stacks across modifier' key to move *all* items (not just matching ones) across inventories
  
  Improved 'Deposit cursor stack on release' behaviors.
  
  Organized settings into categories.
  
  Added option to disable translation fetching.
- 1.2.4 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.2.3 (14 Jan. 2021): New feature:
  - "Choose bottom row stacks first": When you use 'Drag matching stacks across inventories', stacks will be moved from the bottom of inventories first.
  Also improved "Deposit cursor stack on release" so that if there's no room to deposit the cursor stack in the new inventory, the stack will be put back in the old inventory. 
  1.16.1 version only: fixed bundling the wrong version of cloth config
- 1.2.2 (13 Jan. 2021): Fixed some issues where items would shift-click to your offhand when they shouldn't. 
- 1.2.1 (12 Jan. 2021): New (compatibility) feature: 
  - "Armor swap item blacklist", default is empty. 
  
  Fixed armor swapping when armor is in offhand. 
- 1.2 (12 Jan. 2021): New features!
  - "When comparing stacks, ignore NBT", default is No/false
  - "Deposit cursor stack on release", default is 'AT_EMPTY_SLOT'
    This affects what happens when you let go after using the existing "Drag matching stacks across inventories" feature
  - "Drag single stack out of inventory", default is Yes/true
  - "Drag matching stacks out of inventory", default is 'WITH_KEY'
    Configure the (optional) key from Minecraft's controls interface
- 1.1.1 (20 Dec. 2020): Prevent the shift-click features from interfering when the player has an external inventory open, 
  so items that are offhand-preferred can be shift-clicked between different inventories like other items. 
  
  There's a known issue in this and version and 1.1:
  Sometimes when using pick-block in creative, the player's main-hand block will de-sync with the server, so they won't 
  actually be able to place the block they've picked (it will appear to be placed momentarily, but instantly disappear). 
  
  If you've experienced this issue and have some idea of how to reproduce it, please leave a comment or, better yet, 
  create an issue on the [issue tracker](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues), it will be much appreciated!
- 1.1 (8 Dec. 2020): New features! 
  - Pick block fills stack: If you pick-block while you already have the picked block selected, the stack in your hand 
    will be refilled from your inventory. 
  - Pick block never changes slot: If you pick-block while the block you're pointing at is on your hotbar, instead of 
    changing which slot you have selected, that block will be swapped to your hand. 
  - Drag matching stacks across inventories: With an external inventory open (chest, dropper, etc. ), click, hold, and 
    drag a stack between the two inventories to move all matching stacks. 
  
  Added mod icon (visible in Mod Menu's mod list). 
  
  Translations are now handled with [CrowdinTranslate](https://github.com/gbl/CrowdinTranslate), go [here](https://crowdin.com/project/inventory-control-tweaks) if you'd like to contribute a translation. 
- 1.0.0 (20 Nov. 2020): Initial release version! 
- 0.0.1: Initial version. 
