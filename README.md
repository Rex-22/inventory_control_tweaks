Minecraft 1.16 mod for the [Fabric Mod Loader](https://www.fabricmc.net/). 

This mod makes various changes to inventory controls to make them more convenient. 

Known compatibility issue: if you use [Item Scroller](https://www.curseforge.com/minecraft/mc-mods/item-scroller), 
Inventory Control Tweaks' shift-click features won't work. There won't be a crash, and the Armor Swap feature will still work. 
This may be dependent on Item Scroller settings. I'm looking into a compat fix. 

Current features:
- ##### Armor swap
Right click while holding a piece of armor to swap it with the piece you're currently wearing.

You can also swap by clicking armor in your inventory while holding a modifier key that can be configured in Minecraft's controls options.

![armor_swap](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/armor_swap.gif) ![elytra_swap](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/elytra_swap.gif) ![piglin_gold_swap](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/piglin_gold_swap.gif)
- ##### Armor swap item blacklist
Using items in this list won't swap them with your current armor. Useful for compatibility.
- ##### Pick block fills stack
If you pick-block (middle click) while you already have the picked block selected, the stack in your hand will be refilled from your inventory.
- ##### Pick block never changes slot
If you pick-block and the block you're pointing at is on your hotbar, instead of changing which slot you have selected, that block will be swapped to your hand.

![pick_block_fills_stack](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/pick_block_fills_stack.gif) ![pick_block_never_changes_slots](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/pick_block_never_changes_slots.gif)
- ##### When comparing stacks, ignore NBT
- ##### Drag matching stacks across inventories
With an external inventory open (chest, dropper, etc. ), click, hold, and drag a stack between the two inventories to move all matching stacks.

You can also drag all stacks (not just matching) if you hold anther modifier key, configurable in Minecraft's controls options.

![drag_across_ignore_nbt](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_across_ignore_nbt.gif)
- ##### Deposit cursor stack on release
When you use 'Drag matching stacks across inventories', the stack you were dragging will be deposited upon release.
Possible values: "DISABLED", "AT_HOVERED_SLOT", "AT_EMPTY_SLOT"
- ##### Drag single stack out of inventory
With an inventory open, click, hold, and drag a stack outside the inventory to throw the stack.

![drag_single](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_single.gif)
- ##### Drag matching stacks out of inventory
With an inventory open, click, hold, and drag a stack outside the inventory to throw it and all matching stacks.
Optionally requires a key to be held which can be configured in Minecraft's controls interface.
Possible values: "DISABLED", "NO_KEY", "WITH_KEY"

![drag_matching](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_matching.gif) ![drag_out_ignore_nbt](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_out_ignore_nbt.gif)
- ##### Shift-click to partial offhand stack
If a partial stack of items is in your offhand, shift-clicking another stack of the same type of items will add them to your offhand.
- ##### Shift-click any food to offhand
Shift-clicking any food in your inventory while your offhand is empty will move that food to your offhand.
- ##### Don't shift-click food items with negative effects to offhand
Excludes food with negative effects from shift-clicking to your offhand.
Examples: rotten flesh, raw chicken, spider eye. This won't check suspicious stews because you're not supposed to know their effects. 
- ##### List of items that shift-click to offhand
Shift-clicking items in this list while your offhand is empty will move those items to your offhand.

![shift_click_food](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/shift_click_food.gif) ![offhand_preferred](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/offhand_preferred.gif)
  
Each of these features can be configured either through [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu/files/all?filter-game-version=1738749986%3a70886) or by editing `.minecraft/config/inventory_control_tweaks.json`

The default settings are
```json
{
    "enableArmorSwap": true,
    "armorSwapBlackList": [],
    "enablePickFillsStack": true,
    "enablePickNeverChangesSlot": true,
    "ignoreStackNbt": false,
    "depositCursorStackOnRelease": "AT_EMPTY_SLOT",
    "dragSingleStackOutOfInventory": true,
    "dragMatchingStacksOutOfInventory": "WITH_KEY",
    "enableShiftClickToOffhandStack": true,
    "allFoodIsOffhandPreferred": true,
    "excludeFoodWithNegativeEffects": true,
    "offhandPreferredItems": [
        "minecraft:shield",
        "minecraft:firework_rocket",
        "minecraft:totem_of_undying"
    ]
}
```

If you'd like to contribute a translation, follow [this link](https://crowdin.com/project/inventory-control-tweaks). Once approved, a new translation will be available without the need to download an update thanks to [CrowdinTranslate](https://github.com/gbl/CrowdinTranslate). 

[![Requires the Fabric API](https://i.imgur.com/Ol1Tcf8.png)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986%3a70886)

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it. 

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required. 
